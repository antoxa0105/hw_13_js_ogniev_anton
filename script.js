theme.setAttribute("href", window.localStorage.getItem("currentTheme") || "./Styles/style.css");

document.querySelector(".btn-theme").addEventListener("click", function () {
  let theme = document.getElementById("theme");
  if (theme.getAttribute("href") == "./Styles/style.css") {
    theme.href = "./Styles/style_light.css";
  } else {
    theme.href = "./Styles/style.css";
  }
});

window.addEventListener("beforeunload", function () {
  this.window.localStorage.setItem("currentTheme", theme.getAttribute("href"));
});
